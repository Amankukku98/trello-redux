import React, { Component } from 'react';
import { Box, Flex, Heading, Button, Input } from '@chakra-ui/react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import AppsIcon from '@mui/icons-material/Apps';
class Navbar extends Component {
    render() {
        return (
            <Box>
                <Flex bg="#026AA7" alignItems="center" w="100%" >
                    <Heading color="white" ml={5} display="flex" mt={0} fontSize={24}><AppsIcon />Trello</Heading>
                    <Button bg="#026AA7" border="none" color="white" fontSize={16} ml={12}> <p>Workspaces</p><ExpandMoreIcon /></Button>
                    <Button bg="#026AA7" border="none" color="white" fontSize={16} mt={0} ml={12}> <p>Recent</p><ExpandMoreIcon /></Button>
                    <Button bg="#026AA7" border="none" color="white" fontSize={16} mt={0} ml={12}> <p>Starred</p><ExpandMoreIcon /></Button>
                    <Button bg="#026AA7" border="none" color="white" fontSize={16} mt={0} ml={12}> <p>Templates</p><ExpandMoreIcon /></Button>
                    <Button colorScheme='blue' fontSize={18} h={35} mt={0} bg="#0000004d" ml={10} p="1rem 2rem" border="none" color="white">Create</Button>
                    <Box display="flex" ml="55rem">
                        <Box color="black" bg="white" borderRadius={10} display="flex" alignItems="center" h={5} mt={4} mb={2}>
                            <Input type="text" placeholder='Search' border="none" variant='styled' color="red" w={200} />
                        </Box>
                        <Box bg="#026AA7" border="none" color="white" mt={18} ml={15}>
                            <InfoOutlinedIcon />
                        </Box>
                        <Box bg="#026AA7" border="none" color="white" mt={18} ml={15}>
                            <NotificationsNoneOutlinedIcon />
                        </Box>
                        <Box bg="#f59927cc" borderRadius={100} w={45} h={45} textAlign="center" mt={1} mb={1} ml={18} p={2.5} mr={5}><p>AK</p></Box>
                    </Box>
                </Flex>
            </Box >
        )
    }
}
export default Navbar;