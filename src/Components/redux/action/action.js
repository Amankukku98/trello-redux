import { Board_Success, Board_Failure, Create_Board } from "./actionType";


export const getBoardsSuccess = (boards) => {
    return {
        type: Board_Success,
        payload: boards,
    }
}

export const getBoardsFailure = (error) => {
    return {
        type: Board_Failure,
        payload: error,
    }
}

export const createBoard = (board) => {
    return {
        type: Create_Board,
        payload: board,
    }
}