import { Board_Success, Board_Failure, Create_Board } from "../action/actionType";

const initialBoardState = {
    boards: [],
    isLoading: true,
    error: ''
}

export const boardReducer = (state = initialBoardState, action) => {
    switch (action.type) {
        case Board_Success:
            return {
                ...state,
                isLoading: false,
                boards: action.payload,
                error: '',
            }
        case Board_Failure:
            return {
                ...state,
                isLoading: false,
                boards: [],
                error: action.payload
            }
        case Create_Board:
            const newBoard = state.boards.concat(action.payload)
            return {
                ...state,
                boards: newBoard,
            }
        default: return state
    }
}