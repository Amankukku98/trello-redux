import { legacy_createStore as createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { boardReducer } from './reducers/bordReducer';

export const store = createStore(boardReducer, applyMiddleware(thunk));