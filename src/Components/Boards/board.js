import React from 'react';
import { connect } from 'react-redux';
import * as TrelloApi from '../../api';

import { getBoardsSuccess, getBoardsFailure, createBoard } from '../redux/action/action';
import {
    Box, Heading, Button,
    Input,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
} from '@chakra-ui/react';

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            boardName: '',
        }
    }
    componentDidMount() {
        this.props.getAllBoards();
    }

    createBoard = () => {
        const { boardName } = this.state;
        this.props.createBoard(boardName);
        this.state.isOpen = false;
    }
    handleInput = (event) => {
        this.setState({ boardName: event.target.value });
    }

    handleOpen = () => {
        this.setState({ isOpen: true });
    }
    handClose = () => {
        this.setState({ isOpen: false });
    }

    render() {
        if (this.props.isLoading) {
            return (
                <h1>Loading...</h1>
            )
        }

        if (this.props.error) {
            return (
                <h1>Failed to Load Boards Data</h1>
            )
        }

        if (this.props.boards !== undefined) {
            return (
                <Box display="flex" flexWrap="wrap">
                    {this.props.boards.map((eachBoard, index) => {
                        return (
                            <Box display="flex" flexWrap="wrap" key={index} backgroundColor="#87CEFA" m="10px" p="10px" h="10rem" w="20rem" ml="2rem" mt="5rem" color="white" justifyContent="center" alignItems="center" border="1px">
                                <Box>
                                    <Heading as="h1" fontSize="28px">{eachBoard.name}</Heading>
                                </Box >
                            </Box>
                        )
                    })
                    }
                    <Box mt="5rem" ml="2rem">
                        <>
                            <Box bg="gray.200" w="20rem" h="10rem" textAlign="center" onClick={this.handleOpen}>
                                <Button bg="gray.200" mt={14}> Create new board</Button>
                            </Box>

                            <Modal isOpen={this.state.isOpen}>
                                <ModalOverlay />
                                <ModalContent mt="19rem">
                                    <ModalHeader>Create a board</ModalHeader>
                                    <ModalBody>
                                        <Input placeholder='Enter board title' onChange={this.handleInput}></Input>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button colorScheme='blue' mr={3} onClick={this.handClose}>
                                            Close
                                        </Button>
                                        <Button variant='ghost' onClick={this.createBoard} color="white" bg="blue.300">Create</Button>
                                    </ModalFooter>
                                </ModalContent>
                            </Modal>
                        </>
                    </Box>
                </Box>
            )

        }
    }
}
const mapStateToProps = (state) => {
    return {
        boards: state.boards,
        isLoading: state.isLoading,
        error: state.error,
    }
}
const mapDispatchToProps = (dispatch) => ({
    getAllBoards: () => TrelloApi.getAllBoards()
        .then(boards => {
            return dispatch(getBoardsSuccess(boards))
        }).catch(error => {
            dispatch(getBoardsFailure(error))
        }),
    createBoard: (boardName) => TrelloApi.createBoards(boardName)
        .then(res => {
            return dispatch(createBoard(res))
        })
})
export default connect(mapStateToProps, mapDispatchToProps)(Board);