import React from 'react';
import Board from './Components/Boards/board';
import Navbar from './Components/Navbar/Navbar';
class App extends React.Component {
  render() {
    return (
      <>
        <Navbar />
        <Board />
      </>
    )
  }
}
export default App;