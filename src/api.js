import axios from "axios";
const key = '52af232059e7ee61a08e8f00267de1f7';
const token = '62775fdf3a76de291dbe507c071304f3ff6ffac5b4319d555b2d0f33b35d533a';

export const getAllBoards = () => {
    return axios.get(`https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`)
        .then(res => res.data);
}

export const createBoards = (name) => {
    return axios.post(`https://api.trello.com/1/boards/?name=${name}&key=${key}&token=${token}`)
        .then(res => res.data);
}